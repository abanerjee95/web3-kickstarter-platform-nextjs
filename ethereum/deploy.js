const HDWalletProvider = require('@truffle/hdwallet-provider');
const Web3 = require('web3');
const campaignFactory = require('./build/CampaignFactory.json');

provider = new HDWalletProvider(
    'permit truth dwarf limb hunt million wide victory involve rough escape unable',
    'https://ropsten.infura.io/v3/ed206b90ad5a4ecbb66fde5c706ab90c'
  );
  
const web3 = new Web3(provider);

const deploy = async () => {
  const accounts = await web3.eth.getAccounts();

  console.log('Attempting to deploy from account', accounts[0]);
  gasEstimate = (await web3.eth.estimateGas({ data: campaignFactory.evm.bytecode.object })) + 100000;
  const result = await new web3.eth.Contract(campaignFactory.abi)
    .deploy({ data: campaignFactory.evm.bytecode.object })
    .send({ gas: gasEstimate, from: accounts[0] });

  console.log('Contract deployed to', result.options.address);
  provider.engine.stop();
};
deploy();