import React,{useState} from "react";
import Layout from "../../components/layout";
import { Button, Message, Form, Input } from 'semantic-ui-react'
import factory from '../../ethereum/factory';
import web3 from "../../ethereum/web3";
import {Router} from '../../routes';

const NewCampaign = () => {
    const [minimumContribution, setMinimumContribution] = useState('');
    const [errorMessage, setErrorMessage] = useState('');
    const [loader,setLoader] = useState(false);
    const onSubmit = async (event) => {
        setLoader(true);
        setErrorMessage(''); 
        event.preventDefault();
        try{
        const accounts = await web3.eth.getAccounts();
        await factory.methods.createCampaign(web3.utils.toWei(minimumContribution, 'ether')).send({from: accounts[0]});
        setLoader(false);
        Router.pushRoute('/');
        }
        catch(err){
            setErrorMessage(err.message)
            setLoader(false);
        }
    }

    return (
        <Layout>
        <h1>New Campaign</h1>
        <Form onSubmit={onSubmit} error={errorMessage.length>0}>
            <Form.Field>
                <label>Minimum Contribution</label>
                <Input
                label="eth"
                placeholder="Minimum amount of ether for contributors to participate."
                labelPosition="right"
                value={minimumContribution}
                onChange={e=>setMinimumContribution(e.target.value)}
                />
            </Form.Field>
            <Message
            error
            header="Oops!"
            content={errorMessage}/>
            <Button loading={loader} primary type='submit' >Create</Button>
        </Form>
        
        </Layout>
    );
}

export default NewCampaign;