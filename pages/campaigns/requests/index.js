import React from "react";
import Layout from "../../../components/layout";
import { Button,Card,Grid,Table  } from "semantic-ui-react";
import {Link} from '../../../routes';
import campaign from '../../../ethereum/campaign';
import web3 from "../../../ethereum/web3";
import RequestRow from "../../../components/requestRow";

const RequestsIndex = (props) => {
    const {Header, Row,HeaderCell, Body} = Table;
    const renderRows = () => {
        return props.requests.map((request, index) => {
            return (
              <RequestRow
                key={index}
                id={index}
                request={request}
                address={props.address}
                approversCount={props.approversCount}
              />
            );
          });
    }
    return (
    <Layout>
        <h3>Requests</h3>
        <Link route={`/campaigns/${props.address}/requests/new`}>
            <a>
                <Button primary floated="right" content="Add Request" icon="add" style={{marginBottom:'10px'}}/>
            </a>
        </Link>
        <Table>
            <Header>
                <Row>
                    <HeaderCell>ID</HeaderCell>
                    <HeaderCell>Description</HeaderCell>
                    <HeaderCell>Amount</HeaderCell>
                    <HeaderCell>Recipient</HeaderCell>
                    <HeaderCell>Approval Count</HeaderCell>
                    <HeaderCell></HeaderCell>
                    <HeaderCell></HeaderCell>

                </Row>
                
            </Header>
            <Body>
            {renderRows()}
            </Body>
        </Table>
        <div>Found {props.requestCount} requests</div>
    </Layout>);
}

RequestsIndex.getInitialProps = async (props) => {
    const Campaign = campaign(props.query.address);
    const requestCount = await Campaign.methods.getRequestCount().call();
    const approversCount = await Campaign.methods.approversCount().call();
    const requests = await Promise.all(
        Array(parseInt(requestCount)).fill().map((element,index) => {
            return Campaign.methods.requests(index).call();
        })
    );
    return {
        address: props.query.address,
        requests,
        requestCount,
        approversCount
    };
}


export default RequestsIndex;