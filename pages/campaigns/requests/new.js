import React,{useState} from "react";
import Layout from "../../../components/layout";
import { Button, Message, Form, Input } from 'semantic-ui-react'
import web3 from "../../../ethereum/web3";
import campaign from "../../../ethereum/campaign";

const RequestNew = (props)=>{
    const [amount, setAmount] = useState("");
    const [description, setDescription] = useState("");
    const [recipient, setRecipient] = useState("");
    const [loader, setLoader] = useState(false);
    const [errorMessage, setErrorMessage] = useState("");
    const onSubmit = async (event)=>{
        event.preventDefault();
        setLoader(true);
        setErrorMessage("");
        try{
            const accounts = await web3.eth.getAccounts();
            const Campaign = campaign(props.address);
            await Campaign.methods.createRequest(
                description,
                web3.utils.toWei(amount, 'ether'),
                recipient).send({
                from: accounts[0]
            });
            setLoader(false);
        }catch(err){
            setLoader(false);
            setErrorMessage(err.message);
        }
    }
    return (
        <Layout>
            <h1>Create a Request</h1>
            <Form onSubmit={onSubmit} error={errorMessage.length>0}>
            <Form.Field>
                <label>Description</label>
                <Input 
                placeholder="Enter your purpose for creating this request"
                value={description}
                onChange={e => setDescription(e.target.value)}
                />
            </Form.Field>
            <Form.Field>
                <label>Amount</label>
                <Input
                label="ether"
                placeholder="Amount to be requested for transfer"
                labelPosition="right"
                value={amount}
                onChange={e=>setAmount(e.target.value)}
                />
            </Form.Field>
            <Form.Field>
                <label>Recipient</label>
                <Input
                placeholder="Enter the address of the recipient"
                value={recipient}
                onChange={e=>setRecipient(e.target.value)}
                />
            </Form.Field>
            <Message
            error
            header="Oops!"
            content={errorMessage}/>
            <Button loading={loader} primary type='submit' >Create</Button>
        </Form>
        </Layout>
    )
}

RequestNew.getInitialProps = async (props) => {
    return {
        address: props.query.address,  
    };
}

export default RequestNew;