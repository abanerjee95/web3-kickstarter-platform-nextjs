import React from "react";
import web3 from "../../ethereum/web3";
import Layout from "../../components/layout";
import campaign from "../../ethereum/campaign";
import { Button, Card,Grid } from 'semantic-ui-react';
import ContributeForm from "../../components/contributeForm";
import {Link} from '../../routes';

const CampaignShow = (props) => {
    
    const renderCards = () => {
        const items = [
            {
                header: web3.utils.fromWei(props.minumumContribution, 'ether'),
                description: 'Minimum Contribution',
                meta: 'ether',
                style: { overflowWrap: 'break-word' }
            },
            {
                header: web3.utils.fromWei(props.balance, 'ether'),
                description: 'Balance',
                meta: 'ether',
                style: { overflowWrap: 'break-word' }
            },
            {
                header: props.requestsCount,
                description: 'Number of Requests',
                style: { overflowWrap: 'break-word' }
            },
            {
                header: props.approversCount,
                description: 'Number of Approvers',
                style: { overflowWrap: 'break-word' }
            },
            {
                header: props.manager,
                description: 'Manager',
                style: { overflowWrap: 'break-word' }
            }
        ];

        return <Card.Group items={items} />;
    }

    return (
        <Layout>
            <h3>Campaign Show</h3>
            <Grid>
                <Grid.Row>
                <Grid.Column width={10}>
                    {renderCards()}
                </Grid.Column>
                <Grid.Column width={6}>
                    <ContributeForm address={props.address} />
                </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                <Grid.Column>
                    <Link route={`/campaigns/${props.address}/requests`}>
                    <a>
                        <Button primary>
                            View Request
                        </Button>
                    </a>
                    </Link>
                </Grid.Column>
                </Grid.Row>
            </Grid>
            
        </Layout>
        )
}

CampaignShow.getInitialProps = async (props) => {
    const Campaign = campaign(props.query.address);
    const summary = await Campaign.methods.getSummary().call();
    return {
        address: props.query.address,
        minumumContribution: summary[0],
        balance: summary[1],
        requestsCount: summary[2],
        approversCount: summary[3],
        manager: summary[4]
    };
}

export default CampaignShow;