import React,{useState} from "react";
import { Button, Message, Form, Input } from 'semantic-ui-react';
import campaign from '../ethereum/campaign';
import web3 from "../ethereum/web3";
import { Router } from "../routes";

const ContributeForm = (props) =>{
    const [contribution, setContribution] = useState('');
    const [errorMessage, setErrorMessage] = useState('');
    const [loader,setLoader] = useState(false);
    const onSubmit = async (event) => {
        setLoader(true);
        setErrorMessage('');
        event.preventDefault();
        try{
            const Campaign = campaign(props.address);
            const accounts = await web3.eth.getAccounts();
            await Campaign.methods.contribute()
            .send({from: accounts[0], value: web3.utils.toWei(contribution, 'ether')});
            setLoader(false);
            Router.replaceRoute(`/campaigns/${props.address}`);
        } catch(error){
            setErrorMessage(error.message);
            setLoader(false);
        }
    }
    return (
            <Form onSubmit={onSubmit} error={errorMessage.length>0}>
            <Form.Field>
                <label>Amount to Contribute</label>
                <Input
                label="ether"
                placeholder="Amount of ether you want to contribute"
                labelPosition="right"
                value={contribution}
                onChange={e=>setContribution(e.target.value)}
                />
            </Form.Field>
            <Message
            error
            header="Oops!"
            content={errorMessage}/>
            <Button loading={loader} primary type='submit' >Contribute!</Button>
        </Form>
    )
}

export default ContributeForm;