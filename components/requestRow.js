import React,{useState} from "react";
import { Button, Table } from "semantic-ui-react";
import web3 from "../ethereum/web3";
import Campaign from "../ethereum/campaign";

const RequestRow = (props) => {
    const { Row, Cell } = Table;
    const { id, request, approversCount } = props;
    const [loader, setLoader] = useState(false);
    const readyToFinalize = request.approvalCount > approversCount / 2;
    const onApprove = async () => {
        setLoader(true);
        try{
            const campaign = Campaign(props.address);
            const accounts = await web3.eth.getAccounts();
            await campaign.methods.approveRequest(props.id).send({
            from: accounts[0],
            });
            setLoader(false);
        } catch(err){
            setLoader(false);
        }
      };

      const onFinalize = async () => {
        setLoader(true);
        try{
            const campaign = Campaign(props.address);
            const accounts = await web3.eth.getAccounts();
            await campaign.methods.finalizeRequest(props.id).send({
            from: accounts[0],
            });
            setLoader(false);
        } catch(err){
            setLoader(false);
        }
      };

      return (
        <Row disabled={request.complete} positive={readyToFinalize}>
          <Cell>{id}</Cell>
          <Cell>{request.description}</Cell>
          <Cell>{web3.utils.fromWei(request.value, "ether")}</Cell>
          <Cell>{request.recipient}</Cell>
          <Cell>
            {request.approvalCount}/{approversCount}
          </Cell>
          <Cell>
            {request.complete ? null : (
            <Button loading={loader} color="green" basic onClick={onApprove}>
              Approve
            </Button>)
            }
          </Cell>
          <Cell>
          {request.complete ? null : (
            <Button loading={loader} color="red" basic onClick={onFinalize}>
              Finalize
            </Button>)
            }
          </Cell>
        </Row>
      );
}

export default RequestRow;