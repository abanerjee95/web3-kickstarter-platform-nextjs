#### [Blockchain Usecase #1]
## Kickstarter Platform with Web3 and Ethereum 
***

This is a simple Ethereum-based smart-contract web application, demonstrating a blockchain usecase, that allows users to create their campaigns and other users to contribute to them, giving contributers the ability to approve request made by the managers to move balance to another account.

#### Problem Statement:

Amongst the various crowdfunding platforms out there, there has been one constant problem of trust over the contributed project/manager. The contributor's are not able to verify the identity as well as the usage of contributed funds since they have no control once contributed.

#### Solution:

* Decentralized approach gives the contrubuters the ability to verify the identity of the contributor and the usage of contributed funds.
* Giving contributers the ability to approve request to move funds for project specific requirements.
* Inability of the manager to move funds at his whim.

#### Dependencies:

* [Metamask Chrome Extension](https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn?hl=en) 
* Some Eth in your MetaMask wallet/accounts for testing.
    * Ropsten TestNet Faucet
        * [Dimension Network](https://faucet.dimensions.network/)
        * [Egorfine](https://faucet.egorfine.com/)

#### Installation:

`npm run install or yarn install`

If getting legacy peer dependency error, run `npm run install --legacy-peer-deps`

#### Application:
First, run the development server:
```
npm run dev
# or
yarn dev
```
Open http://localhost:3000 with your browser to see the result.

Below is the list of the routes:

* **List of open campaigns: (path: /)**
---
![Campaign List](./images/all_campaign.png)

---
* **Create new campaign: (path: /new)**
![New Campaign](./images/new_campaign.png)

---
* **View campaign details: (path: /campaigns/:address)**
---
![Campaign details](./images/campaign_details.png)

---
* **Add new request: (path: /campaigns/:address/requests/new)**
---
![Add Request](./images/new_request.png)

---
* **List of all open requests: (path: /campaigns/:address/requests)**
---
![All Requests](./images/all_requests.png)

---
* **Approved request (Approval count % > 50%): (path: /campaigns/:address/requests)**
---
![Approved Request](./images/approved_request.png)

---
* **Etherscan transaction on Finalizing request by manager:**
---
![Etherscan transaction](./images/etherscan_trans.png)

---